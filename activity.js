let http = require('http');

const port = 3000;

const server = http.createServer((req, res) => {
    if(req.url == '/login'){
        res.writeHead(200, {'Context-Type' : 'text/plain'})
        res.end(`Welcome to the login page`)
    }
    else if(req.url == '/register'){
        res.writeHead(200, {'Context-Type' : 'text/plain'})
        res.end(`I'm sorry the page you are looking for cannot be found`)
    }
    else{
        res.writeHead(404, {'Context-Type' : 'text/plain'})
        res.end(`Error. page under maintenance`)
    }
});

server.listen(port);

console.log(`Server now accessible at localhost: ${port}`)